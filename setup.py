#!/usr/bin/env python2
from shutil import copyfile
import os
from setuptools import setup, find_packages
setup(
    name="gabriel",
    version="0.2",
    packages=find_packages(),

    install_requires=[
        'python-daemon>2.0',
        'pyyaml>3.0',
        'psutil>5.0'],

    entry_points={
        'console_scripts': [
            'gabriel = gabriel.gabriel:daemon_run'
        ]
    },

    author="Mikolaj Niedbala",
    author_email="kontakt@mikolajniedbala.pl",
    description="Watcher for OS linux system services",
    license="GNU Library or Lesser General Public License (LGPL)",
    url="https://dev.metatron.com.pl",

)
if not os.path.exists('/etc/gabriel'):
    os.mkdir('/etc/gabriel')

if not os.path.exists('/etc/gabriel/config.yaml'):
    copyfile('config.yaml', '/etc/gabriel/config.yaml')
