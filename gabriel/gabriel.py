#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Daemon to monitor system service in linux os
"""

from __future__ import print_function

import argparse
import multiprocessing
import os
from signal import SIGTERM

from daemon import DaemonContext
from daemon import pidfile

from .elements.config_parser import parse_config
from .elements.exceptions import LackOfDefinedServices
from .watcher import Watcher


def daemon_establish():
    """
    Function to run daemon of watcher
    """
    config = parse_config('/etc/gabriel/config.yaml')
    list_services_to_watch = config.get('servicesToWatch')

    if not list_services_to_watch:
        raise LackOfDefinedServices('Define some services to watch')

    watch = Watcher(config)
    watchers = list()
    for service in list_services_to_watch:
        process = multiprocessing.Process(name=service,
                                          target=watch.watch_if_process_exists,
                                          args=(service,))
        watchers.append(process)
        process.daemon = True
        process.start()

    for watcher in watchers:
        watcher.join()


def daemon_start(pid):
    """
    Function to start daemon
    """
    with DaemonContext(pidfile=pidfile.TimeoutPIDLockFile(pid)):
        try:
            daemon_establish()
        except LackOfDefinedServices as error:
            print(error.message)
            exit(1)


def daemon_stop(pid):
    """
    Function to stop daemon
    """
    if os.path.exists(pid) and os.path.isfile(pid):
        with open(pid, 'r') as pid_file:
            pid_number = pid_file.readline()
        os.kill(int(pid_number), SIGTERM)
    else:
        print('Gabriel daemon is not running.')


def daemon_status(pid):
    """
    Function to check daemon pid file status
    """
    if os.path.exists(pid) and os.path.isfile(pid):
        with open(pid, 'r') as pid_file:
            pid_number = pid_file.readline()
        print('Gabriel daemon runs. PID: {}'.format(pid_number))
    else:
        print('Gabriel daemon is not running.')


def daemon_run():
    """
    Parse args and call proper function
    """
    parser = argparse.ArgumentParser(description='Daemon to monitor system service in linux os')
    parser.add_argument('--start', help='Starts daemon', action='store_true')
    parser.add_argument('--stop', help='Stops daemon', action='store_true')
    parser.add_argument('--status', help='Checks daemon status', action='store_true')
    parser.add_argument('--pid', help='Define pid file location', default='/var/run/gabriel.pid')
    args = parser.parse_args()

    if args.start:
        daemon_start(args.pid)
    elif args.stop:
        daemon_stop(args.pid)
    elif args.status:
        daemon_status(args.pid)
    else:
        print('No run parameter specified, use help for this script: gabriel --help')
        exit(1)


if __name__ == "__main__":
    daemon_run()
