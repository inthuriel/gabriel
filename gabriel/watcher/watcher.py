import subprocess
from time import sleep

import psutil

from gabriel.elements.exceptions import ProcessNotFound, NotSupportedOperationSystem, \
    RestartProcessFailure
from gabriel.elements.helpers import notify_by_plugin
from gabriel.elements.processes import SystemProcesses


class Watcher(object):
    """
    Class to watch system service in linux os
    """
    def __init__(self, config_object):
        self.__processes = SystemProcesses()
        self.__config = config_object
        self.__notify = config_object.get('notifyOnFailure', True)
        self.__system_type = self.__check_for_system_init()

    def watch_if_process_exists(self, process_name):
        """
        method to check if process with given name is present in process tree
        :param process_name:
        """

        restart_attempt = 0
        restart_service = False
        restart_attempts_limit = self.__config.get('restartAttempts', 1)
        failure_notified = False
        while True:
            if not restart_service or restart_attempt >= restart_attempts_limit:
                try:
                    self.__processes.check_process_by_name(process_name)
                except ProcessNotFound as error:
                    if self.__notify and not failure_notified:
                        notify_by_plugin(config=self.__config,
                                         process_name=process_name,
                                         attempt=restart_attempt,
                                         message_level='ERROR',
                                         subject='[{}] Service {} is not running'.format(
                                             'ERROR', process_name),
                                         message=error.message)
                        failure_notified = True
                    restart_service = True

            else:
                if restart_attempt < restart_attempts_limit:
                    restart_attempt += 1
                    try:
                        self.__attempt_to_start_process(process_name,
                                                        restart_attempt,
                                                        self.__system_type)
                    except RestartProcessFailure:
                        pass
                    else:
                        restart_attempt = 0
                        restart_service = False

                    if restart_attempt == restart_attempts_limit and self.__notify:
                        notify_by_plugin(config=self.__config,
                                         process_name=process_name,
                                         attempt=restart_attempt,
                                         message_level='ERROR',
                                         subject='[{}] Service {} was failed '
                                                 'to restart'.format('ERROR', process_name),
                                         message='Service {} can\'t be restarted after {} '
                                                 'attempts'.format(process_name, restart_attempt))

            sleep(self.__config.get('checkInterval', 5))

    def __attempt_to_start_process(self, process_name, attempt, system_type):

        notify_by_plugin(config=self.__config,
                         process_name=process_name,
                         attempt=attempt,
                         message_level='WARN',
                         subject='[{}] {} attempt to restart service {}'.format('WARN', attempt,
                                                                                process_name),
                         message='{} attempt to restart service {}'.format(attempt, process_name))

        if system_type == 'systemd':
            try:
                subprocess.check_call(['systemctl', 'restart', process_name])
            except subprocess.CalledProcessError:
                raise RestartProcessFailure()
        elif system_type == 'initd':
            try:
                subprocess.check_call(['service', process_name, 'restart'])
            except subprocess.CalledProcessError:
                raise RestartProcessFailure()

        return True

    @staticmethod
    def __check_for_system_init():
        process = psutil.Process(1)
        process_name = process.name()

        if process_name not in ['systemd', 'initd']:
            raise NotSupportedOperationSystem('System type is: {} and its not supported'.format(
                process_name))

        return process_name
