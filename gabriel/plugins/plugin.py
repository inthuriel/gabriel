from gabriel.elements.exceptions import PluginExecutionError


class HaronPlugin(object):
    """
    Base class for plugins
    """
    def __init__(self):
        pass

    @staticmethod
    def run(**kwargs):
        """
        Default run() method for plugin
        :param kwargs:
        :return:
        """
        raise PluginExecutionError('Yours plugin has not run() method defined. '
                                   'It can\'t be started.')
