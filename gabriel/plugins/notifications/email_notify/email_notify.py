"""
Email plugin for Gabriel Watcher
Author Mikolaj Niedbala
"""
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP

from gabriel.plugins.plugin import HaronPlugin


class EmailNotify(HaronPlugin):
    """
    Class to send watcher messages via email
    """
    @staticmethod
    def run(**kwargs):
        """
        default run() method to run the plugin
        :param kwargs:
        :return:
        """
        process_name = kwargs.get('process_name', 'Undefined')
        attempt = kwargs.get('attempt', 'unknown')
        subject = kwargs.get('subject', 'Service {} is not running'.format(process_name))
        message_text = kwargs.get('message', 'Service {} can\'t be restarted '
                                             'after {} attempts'.format(process_name, attempt))
        recipient = kwargs.get('mail', 'test@test.co.ltd')

        server_address = kwargs.get('server', 'localhost')
        server_port = kwargs.get('port', 25)
        server_user = kwargs.get('user')
        server_password = kwargs.get('password')
        sender_address = kwargs.get('sender_address', 'gabriel@localdomain.co.ltd')

        if isinstance(recipient, str):
            recipient = [recipient]

        message = MIMEMultipart()
        message['From'] = sender_address
        message['To'] = ', '.join(recipient)
        message['Subject'] = subject
        message.attach(MIMEText(message_text, 'plain'))

        server = SMTP(server_address, server_port)
        server.ehlo()
        server.starttls()

        if server_user:
            server.login(server_user, server_password)

        try:
            server.sendmail(sender_address, recipient, message.as_string())
        finally:
            server.quit()
