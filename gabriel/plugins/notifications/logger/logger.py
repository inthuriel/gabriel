"""
Logging plugin for Gabriel Watcher
Author Mikolaj Niedbala
"""
import logging
import logging.config
import logging.handlers

from gabriel.elements.helpers import Singleton
from gabriel.plugins.plugin import HaronPlugin


class Logger(HaronPlugin):
    """
    Class to send watcher messages to logs
    """
    __metaclass__ = Singleton

    def __init__(self):
        super(Logger, self).__init__()
        self.__logger = None

    def run(self, **kwargs):
        """
        default run() method to run the plugin
        :param kwargs:
        :return:
        """
        message_level = kwargs.get('message_level', 'INFO')
        message = kwargs.get('message', 'unknown')
        facility = kwargs.get('logFacility', 'file')
        log_path = kwargs.get('logPath', '/var/log')

        self.__logger_establish(facility, log_path)

        getattr(self.__logger, message_level.lower())(message)

    def __logger_establish(self, facility, path):
        path = path.rstrip('/')

        logging.config.disable_existing_loggers = False
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')

        self.__logger = logging.getLogger(__name__)
        if not getattr(self.__logger, 'handler_set', None):
            self.__logger.setLevel('INFO')
            if facility == 'file':
                handler = logging.FileHandler('{}/gabriel.log'.format(path))
                handler.set_name('gabriel')
                handler.setLevel(logging.INFO)

                handler.setFormatter(formatter)

                self.__logger.addHandler(handler)
                self.__logger.handler_set = True
            elif facility == 'syslog':
                handler = logging.handlers.SysLogHandler()
                handler.set_name('gabriel')
                handler.setLevel(logging.INFO)

                handler.setFormatter(formatter)

                self.__logger.addHandler(handler)
                self.__logger.handler_set = True
