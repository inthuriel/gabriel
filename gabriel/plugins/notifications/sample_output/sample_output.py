"""
Sample plugin for Gabriel Watcher
Author Mikolaj Niedbala
"""
from __future__ import print_function

from gabriel.plugins.plugin import HaronPlugin


class SampleOutput(HaronPlugin):
    """
    Basic plugin returns message to std.io
    """
    @staticmethod
    def run(**kwargs):
        """
        default run() method to run the plugin
        :param kwargs:
        :return:
        """
        process_name = kwargs.get('process_name', 'Undefined')
        attempt = kwargs.get('attempt', 'unknown')
        message_level = kwargs.get('message_level', 'INFO')
        message = kwargs.get('message', '[{}] Service {} fails to start after {} attempts, '
                                        'restart manually'.format(message_level, process_name,
                                                                  attempt))
        print(message)
