# Gabriel plugins
Gabriel supprots plugin system. Currently implemented are `notification` type plugins, but system can be extended by another types of plugins.

## Build plugin
To build plugin create *Python* module in `./plugins` directory in following tree: `{type}.{plugin_name}`

**TL;DR**

- plugin module dir name should be *snake case*,

- plugin main file name should be the same as plugin module dir name,

- plugin main class should be the same as plugin module name, and should be *camel case*,

- plugin main class should inherit `HaronPlugin`

```python
from gabriel.plugins.plugin import HaronPlugin

class SampleOutput(HaronPlugin):
```

- plugin main class should have `run()` method

```python
    def run(**kwargs):
        """
        default run() method to run the plugin
        :param kwargs:
        :return:
        """
```

## Disclaimer
> Feel free to use and modify examples in ./plugins directory