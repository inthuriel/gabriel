from pydoc import locate


def load_plugins(plugin_type, plugin):
    """
    Loads specified plugin and allows to use it
    :param plugin_type:
    :param plugin:
    :return:
    """
    plugin_name = 'gabriel.plugins.{type}.{plugin}.{plugin}'.format(type=plugin_type, plugin=plugin)
    plugin_obj = locate(plugin_name)
    plugin_obj = getattr(plugin_obj, snake_to_camel(plugin))
    return plugin_obj


def get_plugins_by_type(config_object, plugin_type):
    """
    Gets plugins of specified type from object
    :param config_object:
    :param plugin_type:
    :return:
    """
    plugins = config_object.get('plugins', {})
    plugins = plugins.get(plugin_type, {})
    for plugin, plugin_values in plugins.iteritems():
        if not plugin_values:
            plugins[plugin] = dict()

    return plugins


def snake_to_camel(word):
    """
    Transforms snake notation to camel case - file name to class name
    :param word:
    :return:
    """
    return ''.join(x.capitalize() or '_' for x in word.split('_'))
