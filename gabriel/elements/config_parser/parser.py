from __future__ import print_function

import yaml


def parse_config(config_path):
    """
    function to parse watcher config file
    :param config_path:
    :return:
    """
    with open(config_path) as config_file:
        try:
            config_object = yaml.load(config_file)
        except yaml.YAMLError as exception:
            config_object = None
            print(exception.message)
            exit(1)

    return config_object
