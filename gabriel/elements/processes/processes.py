import re
from subprocess import Popen, PIPE

from gabriel.elements.exceptions import ProcessNotFound


class SystemProcesses(object):
    """
    Class to get all processes in process tree in linux os
    """

    @staticmethod
    def __get_all_processes():
        process = Popen(['ps', '-eo', 'pid,args'], stdout=PIPE, stderr=PIPE)
        processes_object = dict()
        stdout, notused = process.communicate()

        for line in stdout.splitlines():
            pid, cmdline = line.strip().split(' ', 1)
            processes_object.setdefault(pid, cmdline)

        return processes_object

    def get_processes(self):
        """
        function to get all processes in process tree in linux os
        :return:
        """
        return self.__get_all_processes()

    def check_process_by_name(self, process_name):
        """
        Function to get process in process tree in linux os by its name
        :param process_name:
        :return:
        """
        current_processes = self.__get_all_processes()
        process_regex = re.compile(process_name, flags=re.IGNORECASE)
        exists = False

        for pid, process in current_processes.iteritems():
            if process_regex.search(process):
                exists = True

        if not exists:
            raise ProcessNotFound('Process {} doesn\'t exists'.format(process_name))

        return exists
