from gabriel.plugin_loader import load_plugins, get_plugins_by_type


def notify_by_plugin(config, process_name, attempt, message_level, subject=None, message=None):
    """
    Run all enabled plugins and send notifications with them
    :param config:
    :param process_name:
    :param attempt:
    :param message_level:
    :param subject:
    :param message:
    :return:
    """
    plugins_list = get_plugins_by_type(config, 'notifications')
    for plugin_name, plugin_values in plugins_list.iteritems():
        plugin_module = load_plugins('notifications', plugin_name)
        plugin = plugin_module()
        plugin.run(process_name=process_name,
                   attempt=attempt,
                   message_level=message_level,
                   subject=subject,
                   message=message,
                   **plugin_values)


class Singleton(type):
    """
    Singleton class
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
