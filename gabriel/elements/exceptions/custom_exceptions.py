class ProcessNotFound(Exception):
    """
    Custom exception for not existing process
    """
    pass


class LackOfDefinedServices(Exception):
    """
    Custom exception for lack of services to watch defined in config
    """
    pass


class PluginNameNotDefined(Exception):
    """
    Custom exception for not defined plugin name
    """
    pass


class PluginExecutionError(Exception):
    """
    Custom exception for plugin execution
    """
    pass


class NotSupportedOperationSystem(Exception):
    """
    Custom exception for OS type
    """
    pass


class RestartProcessFailure(Exception):
    """
    Custom exception for process restart
    """
    pass
