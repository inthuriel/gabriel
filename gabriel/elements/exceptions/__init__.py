from gabriel.elements.exceptions.custom_exceptions import ProcessNotFound, \
    LackOfDefinedServices, PluginExecutionError, PluginNameNotDefined, \
    NotSupportedOperationSystem, RestartProcessFailure
