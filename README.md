# Gabriel

Watcher daemon for linux type OS.

## Installation
```console
sudo pip2.7 install git+https://bitbucket.org/inthuriel/gabriel.git
```

### Configuration
Configuration file is stored in `/etc/gabriel/config.yaml` - all variables are described in comments.

### Usage
```console
~# gabriel --help
usage: gabriel [-h] [--start] [--stop] [--status] [--pid PID]

Daemon to monitor system service in linux os

optional arguments:
  -h, --help  show this help message and exit
  --start     Starts daemon
  --stop      Stops daemon
  --status    Checks daemon status
  --pid PID   Define pid file location
```

## Plugins
Script supports plugins for notifications, short readme is in `./plugins` dir.